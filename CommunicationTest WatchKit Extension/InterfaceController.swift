//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    @IBOutlet var feedBtn: WKInterfaceButton!
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    var pikachuImage : UIImage = UIImage(named: "pikachu")!
    var caterpieImage : UIImage = UIImage(named: "caterpie")!
    
    var hungerLevel : Int = 0
    var healthLevel : Int = 100
    var gameActive :Bool = true
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["choice"] as! String
        if(messageBody == "pokemon"){
            pokemonImageView.setImage(pikachuImage)
        }
        else if(messageBody == "caterpie"){
            pokemonImageView.setImage(caterpieImage)
        }
        
    
        else if(messageBody == "wakeup"){
            feedBtn.setEnabled(true)
            gameActive = true
            increaseLabel()
        }
        gameActive = true
        
        
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        increaseLabel()
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        if let val: String = context as? String {
            self.nameLabel.setText(val)
            self.messageLabel.setText("Hi, i am \(val)")
        } else {
            self.nameLabel.setText("")
            self.messageLabel.setText("")
        }
       
        
    }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
   

   
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        if(healthLevel >= 5 && healthLevel < 101){
            hungerLevel = hungerLevel - 12
            if(hungerLevel >= 80){
                healthLevel = healthLevel - 5
            }
            outputLabel.setText("HP: \(healthLevel)  Hunger: \(hungerLevel)")
        }
        else{
            hungerLevel = 0
            healthLevel = 100
            outputLabel.setText("HP: \(healthLevel)  Hunger: \(hungerLevel)")
        }
        increaseLabel()
        
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        gameActive = false
        feedBtn.setEnabled(false)
        nameLabel.setText("Hibernate mode, press wake up button on phone to resume")
        
    }
    
    func increaseLabel() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            if self.gameActive {
                self.hungerLevel += 10
                if(self.hungerLevel >= 80){
                    if(self.healthLevel > 0 && self.healthLevel < 101){
                    self.healthLevel = self.healthLevel - 5
                  }
                    else if (self.healthLevel <= 0){
                        self.healthLevel = 0
                        self.gameActive = false
                        self.nameLabel.setText("Pokemon is dead")
                    }
                }
                self.outputLabel.setText("HP: \(self.healthLevel)  Hunger: \(self.hungerLevel)")
                self.increaseLabel()
            }
        }
            
        
    }
}
